/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.common.utils.excel.fieldtype;

import java.util.List;

import com.empire.emsite.common.utils.Collections3;
import com.empire.emsite.common.utils.StringUtils;
import com.empire.emsite.modules.sys.entity.Role;
import com.empire.emsite.modules.sys.utils.UserUtils;
import com.google.common.collect.Lists;

/**
 * 类RoleListType.java的实现描述：字段类型转换
 * 
 * @author arron 2017年10月30日 下午7:06:39
 */
public class RoleListType {
    /**
     * 获取对象值（导入）
     */
    public static Object getValue(String val) {
        List<Role> roleList = Lists.newArrayList();
        List<Role> allRoleList = UserUtils.getRoleList();
        for (String s : StringUtils.split(val, ",")) {
            for (Role e : allRoleList) {
                if (StringUtils.trimToEmpty(s).equals(e.getName())) {
                    roleList.add(e);
                }
            }
        }
        return roleList.size() > 0 ? roleList : null;
    }

    /**
     * 设置对象值（导出）
     */
    public static String setValue(Object val) {
        if (val != null) {
            @SuppressWarnings("unchecked")
            List<Role> roleList = (List<Role>) val;
            return Collections3.extractToString(roleList, "name", ", ");
        }
        return "";
    }

}
